<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <title>Mailbiz Lead Ads Test</title>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.5.0/knockout-min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>

     <div class="container">

       <h1 class="my-4">Mailbiz
         <small>Lead Ads Test</small>
       </h1>

       <div class="row">
         <div class="col-md-12">
           <h3>Instalar aplicativo da Mailbiz</h3>
           <center>
              <a data-bind="click: doLogin" class="btn btn-primary" href="#">Conectar com o App Mailbiz</a>
           </center>
         </div>
       </div>

       <hr>

       <div data-bind="if: isLogged" class="row">
         <div class="col-md-6">
            <strong>Selecione uma página</strong>

            <div data-bind="foreach: { data: pages, as: 'page' }" class="list-group">
              <a data-bind="click: $parent.getPageForms(page), text: page.name, css: {active: $parent.selectedPage().id == page.id}" href="#" class="list-group-item list-group-item-action"></a>
            </div>
         </div>
         <div class="col-md-6">
           <strong>Selecione um formulário</strong>

           <div data-bind="foreach: { data: forms, as: 'form' }" class="list-group">
             <a data-bind="click: $parent.getFormData(form.id), text: form.name, css: {active: $parent.selectedForm().id == form.id}" href="#" class="list-group-item list-group-item-action"></a>
           </div>

         </div>
       </div>


       <div data-bind="if: !!selectedForm().id" class="row">
         <div class="col-md-12">
            <strong>Mapear campos do formulário</strong>

            <ul data-bind="foreach: { data: selectedForm().qualifiers, as: 'fields' }" class="list-group">
              <li class="list-group-item">
                <span data-bind="text: fields.label"></span>
                <select style="float: right; width: 55%;" class="form-control" name="fieldMapped[]">
                  <option>Selecione o campo no form da Mailbiz</option>
                  <option>Campo 1 mock</option>
                  <option>Campo 2 mock</option>
                  <option>Campo 3 mock</option>
                </select>
              </li>
            </ul>
         </div>
       </div>

       <div data-bind="if: isLogged" class="row">
        <div class="col-md-12">
          <hr>
          <button type="submit" style="float:right; margin: 10px;" class="btn btn-success">Salvar</button>
        </div>
       </div>

     </div>

  <script>
    function ViewModel() {
      this.isLogged = ko.observable(false);
      this.pages = ko.observableArray();
      this.forms = ko.observableArray();

      this.selectedForm = ko.observable({});
      this.selectedPage = ko.observable({});

      this.doLogin = function() {
        FB.login(function(response){
          console.log('Successfully logged in', response);
          vm.isLogged(response && !response.error);
          FB.api('/me/accounts', function(response) {
            console.log('Successfully retrieved pages', response);
            var pages = response.data || [];
            vm.pages(pages);
          });
        }, {scope: ['manage_pages', 'leads_retrieval']});
      };

      this.getPageForms = function(page) {
        var self = this;
        return function() {
          self.selectedPage(page);
          FB.api(
            '/' + page.id + '/subscribed_apps',
            'post',
            {access_token: page.access_token, subscribed_fields: ['feed']},
            function(response) {
              console.log('Successfully subscribed page', response);

              if(!response || response.error) {
                return alert(response ? response.error : 'Erro ao processar informação');
              }

              getForms(page.id, page.access_token);
            }
          );
        };
      };

      this.getFormData = function(formId) {
        var page = this.selectedPage();
        var token = page.access_token;
        return function() {
          FB.api(
            '/' + formId,
            'get',
            {access_token: token, fields: 'id,name,qualifiers'},
            function(response) {
              console.log('Successfully getFormData', response);

              if(!response || response.error) {
                return alert(response ? response.error : 'Erro ao processar informação');
              }

              vm.selectedForm(response);
            }
          );
        };
      };

      function getForms(pageId, page_access_token) {
        FB.api(
            '/'+ pageId +'/leadgen_forms',
            'get',
            {access_token: page_access_token},
            function (response) {
              console.log('getForms leadgen_forms', response);

              if (response && !response.error) {
                /* handle the result */
                vm.forms(response.data || []);
              }
            }
        );
      }
    }

    var vm = new ViewModel();

    window.fbAsyncInit = function() {
      FB.init({
        appId      : '373855073267407',
        xfbml      : true,
        version    : 'v3.3'
      });

      ko.applyBindings(vm);
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  </script>
</body>

</html>